#!/usr/bin/env python

"""
This just a draft for homework 'near-duplicates'
Use MinshinglesCounter to make result closer to checker
"""

import sys
import re
import mmh3
import itertools
from collections import Counter, defaultdict
from docreader import DocumentStreamReader 

# sys.stderr.write("check1\n")

class MinshinglesCounter:
	SPLIT_RGX = re.compile(r'\w+', re.U)

	def __init__(self, window=5, n=20):
		self.window = window
		self.n = n

	def count(self, text):
		words = MinshinglesCounter._extract_words(text)
		shs = self._count_shingles(words)
		mshs = self._select_minshingles(shs)

		if len(mshs) == self.n:
			return mshs

		if len(shs) >= self.n:
			return sorted(shs)[0:self.n]

		return None

	def _select_minshingles(self, shs):
		buckets = [None]*self.n
		for x in shs:
			bkt = x % self.n
			buckets[bkt] = x if buckets[bkt] is None else min(buckets[bkt], x)

		return filter(lambda a: a is not None, buckets)

	def _count_shingles(self, words):
		shingles = []
		for i in xrange(len(words) - self.window):
			h = mmh3.hash(' '.join(words[i:i+self.window]).encode('utf-8'))
			shingles.append(h)
		return sorted(shingles)

	@staticmethod
	def _extract_words(text):
		words = re.findall(MinshinglesCounter.SPLIT_RGX, text)
		return words


def main():
	mhc = MinshinglesCounter()
	grouped_docs = defaultdict(list)
	doc_id = 0
	urls = []
	for path in sys.argv[1:]:
		for doc in DocumentStreamReader(path):
			shingles = mhc.count(doc.text)
			urls.append(doc.url)
			if shingles is not None:
				for shingle in shingles:
					grouped_docs[shingle].append(doc_id)
			doc_id += 1
	filtered_grouped_docs = []
	for shingle in grouped_docs.keys():
		if (len(grouped_docs[shingle]) > 1):
			filtered_grouped_docs.append(grouped_docs[shingle])
		del grouped_docs[shingle]		
	combinations = []
	for doc_ids in filtered_grouped_docs:
		combinations += itertools.chain(itertools.combinations(doc_ids, 2))
	del filtered_grouped_docs
	counter = Counter(combinations)
	del combinations
	filtered_counter = []
	for ((id1, id2), res) in counter.iteritems():
		if (res > 17 and res <= 20):#mhc.n * 0.75):
			try:
				filtered_counter.append( (id1, id2, float(res) / ( float(res) + 2.0 * (20 - res)))  )
			except ZeroDivisionError:
				continue
				

	for (id1, id2, res) in filtered_counter:
		print "%s %s %d" % (urls[id1], urls[id2], res * 100.0)
	# input()

if __name__ == '__main__':
	main()

