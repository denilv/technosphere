
# coding: utf-8

# In[75]:

import sys
import re
import random
import urllib
import urlparse as up
from collections import Counter
from operator import itemgetter

len_filter = lambda splitted_url: filter(lambda(seg): len(seg) > 0, splitted_url)

def get_sample_urls(path, size = 1000):
    f = open(path, 'r')
    urls = f.read().splitlines()
    if len(urls) < size:
        print path, 'have only', len(urls), 'lines!'
        return []
    sample = random.sample(urls, size)    
    unqouted_urls = [urllib.unquote_plus(url) for url in sample]
    urls = [up.urlparse(url) for url in unqouted_urls]
    return urls

def get_num_of_segments_freqs(urls):
    counter = Counter()
    for url in urls:
        n_segs = len(len_filter(url.path.split('/')))
        counter["segments:%d" % n_segs] += 1
    return counter

def get_params_names_freqs(urls):
    keys = []
    counter = Counter()
    for url in urls:
        keys += up.parse_qs(url.query).keys()
    for i in keys:
        counter["param_name:%s" % i] += 1
    return counter

def get_params_names_freqs(urls):
    keys = []
    counter = Counter()
    for url in urls:
        keys += up.parse_qs(url.query).keys()
    for i in keys:
        counter["param_name:%s" % i] += 1
    return counter

def get_params_values_freqs(urls):
    values = []
    counter = Counter()
    for url in urls:
        params = len_filter(url.query.split('&'))
        values += params
    counter = Counter(values)
    for i in values:
        counter["param:%s" % i] += 1
    return counter

def get_segments_pos_freqs(urls):
    counter = Counter()
    for url in urls:
        segs = len_filter(url.path.split('/'))
        for pos, segment in enumerate(segs):
            counter["segment_name_%d:%s" % (pos, segment)] += 1
    return counter

def get_numeric_segments_pos_freqs(urls):
    counter = Counter()
    for url in urls:
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            if seg.isdigit():
                counter["segment_[0-9]_%d:1" % pos] += 1
    return counter

def get_subnumeric_segments_pos_freqs(urls):
    counter = Counter()
    #??????????????????????????
    num_inside = re.compile('[^\d]+\d+[^\d]+$')
    for url in urls:
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            if num_inside.match(seg):
                counter["segment_substr[0-9]_%d:1" % pos] += 1
    return counter

def get_extension_freqs(urls):
    counter = Counter()
    for url in urls:
        #TODO delete domain like .ru from segments
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            ext_pos = seg.find('.')
            ext_tail = seg[ext_pos + 1:].find('.')
            if (ext_pos != -1) and (ext_tail == -1):
                ext = seg[ext_pos + 1:]
                counter["segment_ext_%d:%s" % (pos, ext)] += 1
    return counter

def get_ext_or_numsubstr_segments_pos_freqs(urls):
    counter = Counter()
    num_inside = re.compile('[^\d]+\d+[^\d]+$')
    for url in urls:
        #TODO delete domain like .ru from segments
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            ext_pos = seg.find('.')
            ext_tail = seg[ext_pos + 1:].find('.')
            if (ext_pos != -1) and (ext_tail == -1) and (num_inside.match(seg)):
                ext = seg[ext_pos + 1:]
                counter["segment_ ext_substr[0-9]_%d:%s" % (pos, ext)] += 1
    return counter

def get_segments_lens_pos_freqs(urls):
    counter = Counter()
    for url in urls:
        #TODO delete domain like .ru from segments
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            counter["segment_len_%d:%d" % (pos, len(seg))] += 1
    return counter

def extract_features(INPUT_FILE_1, INPUT_FILE_2, OUTPUT_FILE):
    output = open(OUTPUT_FILE, 'w')
    
    urls1 = get_sample_urls(path=INPUT_FILE_1, size=1000)
    urls2 = get_sample_urls(path=INPUT_FILE_2, size=1000)
    urls = urls1 + urls2
    
    result = Counter()
    result += get_num_of_segments_freqs(urls)
    result += get_params_names_freqs(urls)
    result += get_params_values_freqs(urls)
    result += get_segments_pos_freqs(urls)
    result += get_numeric_segments_pos_freqs(urls)
    result += get_subnumeric_segments_pos_freqs(urls)
    result += get_extension_freqs(urls)
    result += get_ext_or_numsubstr_segments_pos_freqs(urls)
    result += get_segments_lens_pos_freqs(urls)
    filtered_result = filter(lambda(x) : (x[1] >= 100), result.iteritems())
#         print filtered_result
    sorted_result = sorted(filtered_result, key = itemgetter(1), reverse=True)
#         print sorted_result
    final = ["%s\t%d\n" % i for i in sorted_result]
#    print sorted_result
    for i in final:
        output.write(i)
    output.close()




