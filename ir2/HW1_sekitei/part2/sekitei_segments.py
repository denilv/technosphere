import sys
import os
import re
import random
import time
from sklearn.cluster import Birch
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn.feature_selection import SelectKBest
from sklearn.feature_extraction import DictVectorizer
#from sklearn.cluster import <any cluster algorithm>
import numpy as np
import random
import urllib
import urlparse as up
from collections import Counter
from operator import itemgetter

len_filter = lambda splitted_url: filter(lambda(seg): len(seg) > 0, splitted_url)

def get_sample_urls(path, size = 1000):
    f = open(path, 'r')
    urls = f.read().splitlines()
    if len(urls) < size:
        print path, 'have only', len(urls), 'lines!'
        return []
    sample = random.sample(urls, size)    
    unqouted_urls = [urllib.unquote_plus(url) for url in sample]
    urls = [up.urlparse(url) for url in unqouted_urls]
    return urls

def get_num_of_segments_freqs(urls):
    counter = Counter()
    for url in urls:
        n_segs = len(len_filter(url.path.split('/')))
        counter["segments:%d" % n_segs] += 1
    return counter

def get_params_names_freqs(urls):
    keys = []
    counter = Counter()
    for url in urls:
        keys += up.parse_qs(url.query).keys()
    for i in keys:
        counter["param_name:%s" % i] += 1
    return counter

def get_params_names_freqs(urls):
    keys = []
    counter = Counter()
    for url in urls:
        keys += up.parse_qs(url.query).keys()
    for i in keys:
        counter["param_name:%s" % i] += 1
    return counter

def get_params_values_freqs(urls):
    values = []
    counter = Counter()
    for url in urls:
        params = len_filter(url.query.split('&'))
        values += params
    counter = Counter(values)
    for i in values:
        counter["param:%s" % i] += 1
    return counter

def get_segments_pos_freqs(urls):
    counter = Counter()
    for url in urls:
        segs = len_filter(url.path.split('/'))
        for pos, segment in enumerate(segs):
            counter["segment_name_%d:%s" % (pos, segment)] += 1
    return counter

def get_numeric_segments_pos_freqs(urls):
    counter = Counter()
    for url in urls:
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            if seg.isdigit():
                counter["segment_[0-9]_%d:1" % pos] += 1
    return counter

def get_subnumeric_segments_pos_freqs(urls):
    counter = Counter()
    #??????????????????????????
    num_inside = re.compile('[^\d]+\d+[^\d]+$')
    for url in urls:
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            if num_inside.match(seg):
                counter["segment_substr[0-9]_%d:1" % pos] += 1
    return counter

def get_extension_freqs(urls):
    counter = Counter()
    for url in urls:
        #TODO delete domain like .ru from segments
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            ext_pos = seg.find('.')
            ext_tail = seg[ext_pos + 1:].find('.')
            if (ext_pos != -1) and (ext_tail == -1):
                ext = seg[ext_pos + 1:]
                counter["segment_ext_%d:%s" % (pos, ext)] += 1
    return counter

def get_ext_or_numsubstr_segments_pos_freqs(urls):
    counter = Counter()
    num_inside = re.compile('[^\d]+\d+[^\d]+$')
    for url in urls:
        #TODO delete domain like .ru from segments
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            ext_pos = seg.find('.')
            ext_tail = seg[ext_pos + 1:].find('.')
            if (ext_pos != -1) and (ext_tail == -1) and (num_inside.match(seg)):
                ext = seg[ext_pos + 1:]
                counter["segment_ ext_substr[0-9]_%d:%s" % (pos, ext)] += 1
    return counter

def get_segments_lens_pos_freqs(urls):
    counter = Counter()
    for url in urls:
        #TODO delete domain like .ru from segments
        segs = len_filter(url.path.split('/'))
        for pos, seg in enumerate(segs):
            counter["segment_len_%d:%d" % (pos, len(seg))] += 1
    return counter

def make_url(link):
    unq_url = urllib.unquote_plus(link)
    url = up.urlparse(unq_url)
    return url

def extract_features_from_link(link):
    url = make_url(link)
    urls = [url]
    result = Counter()
    result += get_num_of_segments_freqs(urls)
    result += get_params_names_freqs(urls)
    result += get_params_values_freqs(urls)
    result += get_segments_pos_freqs(urls)
    result += get_numeric_segments_pos_freqs(urls)
    result += get_subnumeric_segments_pos_freqs(urls)
    result += get_extension_freqs(urls)
    result += get_ext_or_numsubstr_segments_pos_freqs(urls)
    result += get_segments_lens_pos_freqs(urls)
#     filtered_result = filter(lambda(x) : (x[1] >= 100), result.iteritems())
#     sorted_result = sorted(filtered_result, key = itemgetter(1), reverse=True)
#     final = ["%s\t%d\n" % i for i in sorted_result]
    return result

BIRCH_BRANCHING_FACTOR = 30
BIRCH_THRESHOLD = 0.25
KBEST_K = 40

algs = []

def define_segments(QLINK_URLS, UNKNOWN_URLS, QUOTA):
    qlink_features = [extract_features_from_link(link) for link in QLINK_URLS]
    unknown_features = [extract_features_from_link(link) for link in UNKNOWN_URLS]
    n_examples = len(QLINK_URLS) + len(UNKNOWN_URLS) #1000
    
    v = DictVectorizer(sparse = False)
    #our features types (v.feature_names_) are at columns and our features are at rows
    trans_x = v.fit_transform(qlink_features + unknown_features)
    q_or_u = np.repeat([1, 0], [len(QLINK_URLS), len(UNKNOWN_URLS)])

    #filter features to get the most valuable feature types
#     best_x = np.array(filter(lambda(column) : np.sum(column) > 0.03 * n_examples, trans_x.T)).T
    true_ind = np.array(map(lambda(column) : np.sum(column) > 0.03 * n_examples, trans_x.T))
    f_ind = np.where(true_ind)[0]
    best_x = trans_x.T[f_ind].T
    m_features = best_x.shape[1]
#     print m_features
    kmeans = KMeans(n_clusters = m_features)
    birch = Birch(branching_factor=BIRCH_BRANCHING_FACTOR, n_clusters=m_features,
                    threshold=BIRCH_THRESHOLD, compute_labels=True)
    clusters2 = birch.fit_predict(best_x)
    cl2 = birch
    clusters1 = kmeans.fit_predict(best_x)
    cl1 = kmeans
    q_ = np.vstack((clusters2, q_or_u)).T
    quota = zip(np.unique(clusters1),
                (np.array([np.sum(q_[q_[:, 0] == c, 1]) for c in np.unique(clusters2)]) / float(len(QLINK_URLS))) * QUOTA * 2)
    quota = {c: int(q) for c, q in quota}
    q2 = quota
    #algs.append({"clustering" : kmeans,
                #"clusters_quotas" : q1,
                #"features_ind" : f_ind,
                #"voc" : v,
                #"quota" : QUOTA})
#     clusters = dbscan.fit_predict(best_x, n_clusters = m_features)
#     print clusters1

    algs.append({"clustering" : cl2,
                "clusters_quotas" : q2,
                "features_ind" : f_ind,
                "voc" : v,
                "quota" : QUOTA})
#     q2 = get_clusters_quotas(clusters2, QUOTA)
#     print clusters2
    

def link2vec(link, voc, f_ind):
    features = extract_features_from_link(link)
    vec_raw = voc.transform(features)[0]
    vec = vec_raw[f_ind]
    return vec
    
def fetch_url(url):
    quota = algs[0]["quota"]
    clustering = algs[0]["clustering"]
    clusters_quotas = algs[0]["clusters_quotas"]
    voc = algs[0]["voc"]
    f_ind = algs[0]["features_ind"]
    vec = link2vec(url, voc, f_ind)
    y = clustering.predict(vec)[0]
    if quota > 0:
        quota -= 1
        if clusters_quotas[y] > 0:
            clusters_quotas[y] -= 1
            return True
        else:
            return False
    else:
        return False
    return False
