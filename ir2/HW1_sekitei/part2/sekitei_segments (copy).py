import sys
import os
import re
import random
import time
from sklearn.cluster import Birch
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn.feature_selection import SelectKBest
from sklearn.feature_extraction import DictVectorizer
#from sklearn.cluster import <any cluster algorithm>
import numpy as np
import random
import urllib
import urlparse as up
from collections import Counter
from operator import itemgetter


algs = []

def define_segments(QLINK_URLS, UNKNOWN_URLS, QUOTA):
    qlink_features = [extract_features_from_link(link) for link in QLINK_URLS]
    unknown_features = [extract_features_from_link(link) for link in UNKNOWN_URLS]
    n_examples = len(QLINK_URLS) + len(UNKNOWN_URLS) #1000
    
    v = DictVectorizer(sparse = False)
    #our features types (v.feature_names_) are at columns and our features are at rows
    trans_x = v.fit_transform(qlink_features + unknown_features)
    q_or_u = np.repeat([1, 0], [len(QLINK_URLS), len(UNKNOWN_URLS)])

    #filter features to get the most valuable feature types
#     best_x = np.array(filter(lambda(column) : np.sum(column) > 0.03 * n_examples, trans_x.T)).T
    true_ind = np.array(map(lambda(column) : np.sum(column) > 0.03 * n_examples, trans_x.T))
    f_ind = np.where(true_ind)[0]
    best_x = trans_x.T[f_ind].T
    m_features = best_x.shape[1]
#     print m_features
    kmeans = KMeans(n_clusters = m_features)
    
    cl1 = kmeans
    q_ = np.vstack((clusters2, q_or_u)).T
    quota = zip(np.unique(clusters1),
                (np.array([np.sum(q_[q_[:, 0] == c, 1]) for c in np.unique(clusters2)]) / float(len(QLINK_URLS))) * QUOTA * 2)
    quota = {c: int(q) for c, q in quota}
    q1 = quota
    #algs.append({"clustering" : kmeans,
                #"clusters_quotas" : q1,
                #"features_ind" : f_ind,
                #"voc" : v,
                #"quota" : QUOTA})
#     clusters = dbscan.fit_predict(best_x, n_clusters = m_features)
#     print clusters1

    algs.append({"clustering" : cl1,
                "clusters_quotas" : q1,
                "features_ind" : f_ind,
                "voc" : v,
                "quota" : QUOTA})
#     q2 = get_clusters_quotas(clusters2, QUOTA)
#     print clusters2
    

def link2vec(link, voc, f_ind):
    features = extract_features_from_link(link)
    vec_raw = voc.transform(features)[0]
    vec = vec_raw[f_ind]
    return vec
    
def fetch_url(url):
    quota = algs[0]["quota"]
    clustering = algs[0]["clustering"]
    clusters_quotas = algs[0]["clusters_quotas"]
    voc = algs[0]["voc"]
    f_ind = algs[0]["features_ind"]
    vec = link2vec(url, voc, f_ind)
    y = clustering.predict(vec)[0]
    if quota > 0:
        quota -= 1
        if clusters_quotas[y] > 0:
            clusters_quotas[y] -= 1
            return True
        else:
            return False
    else:
        return False
    return False
