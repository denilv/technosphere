#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <system_error>
#include <sys/fcntl.h>
#include <strings.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <map>
#include <string.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <sys/time.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
//TODO include server.h

#define BUF_SIZE 1024
#define CHK(eval, str) if(eval < 0) {perror(str); exit(-1);}

using namespace std;

class Client
{
	int cl_fd;
	string address;
	int port;
	struct sockaddr_in cl_sock;
public:
	Client(): cl_fd(0), address(""), port(0) {}
	Client(string addr, int p): cl_fd(0), address(addr), port(p) {}

	int init_client();
};

int Client::init_client()
{
	struct sockaddr_in sock_addr;
	sock_addr.sin_family = AF_INET;
	sock_addr.sin_port = htons(port);
	sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	cl_sock = sock_addr;
	return 0;
}




















