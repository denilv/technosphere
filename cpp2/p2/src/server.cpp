#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <system_error>
#include <sys/fcntl.h>
#include <strings.h>
#include <unistd.h>
#include <vector>
#include <string>
#include <map>
#include <string.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <sys/time.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>

#define MAX_CONNS 10
#define EPOLL_SIZE 50
#define BUF_SIZE 1024
#define CHK(eval, str) if(eval < 0) {perror(str); exit(-1);}

using namespace std;

int setnonblock(int fd)
{
	int flags;
#if defined(O_NONBLOCK)
    if((flags = fcntl(fd, F_GETFL, 0)) ==  -1) {
        flags = 0;
    }
    return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
    flags = 1;
    return ioctl(fd, FIOBIO, &flags);
#endif
}

class Server
{
	int port;
	int serv_fd;
	struct sockaddr_in serv_sock;
	vector<int> clients;
public:

	Server(int p): port(p), serv_fd(0){}

	int main();

	int init_server();

};

int Server::init_server()
{
	struct sockaddr_in sock_addr;
	int _serv_fd = 0;
	int opt_val = 1;

	if ((_serv_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
	{
		perror("creating socket error!\n");
		exit(-1);
	}

	//TODO do with bzero
	memset(&sock_addr, 0, sizeof(sock_addr));
	sock_addr.sin_family = AF_INET;
	sock_addr.sin_port = htons(port);
	sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_sock = sock_addr;

	//some options
	int flags = 0;
	flags = setnonblock(_serv_fd);
	cout << flags << '\n';
	if (setsockopt(_serv_fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof(opt_val)) == -1)
	{
		perror("setsockopt(SO_REUSEADDR) error!\n");
		close(_serv_fd);
		exit(0);
	}

	if (bind(_serv_fd, (struct sockaddr *) &sock_addr, sizeof(sock_addr)) == -1)
	{
		perror("binding socket error!\n");
		close(_serv_fd);
		exit(0);
	}

	if (listen(_serv_fd, MAX_CONNS) == -1)
	{
		perror("listen socket error!\n");
		close(_serv_fd);
		exit(0);
	}

	serv_fd = _serv_fd;

	return 0;
}

int Server::main()
{
	struct epoll_event event;
	int ep_fd;
//	vector<struct epoll_event> events;
	struct epoll_event events[EPOLL_SIZE];
	int n_events, cl_fd, cl_sock;
	char buf[BUF_SIZE];

	event.data.fd = serv_fd;
	event.events = EPOLLIN | EPOLLET;

	ep_fd = epoll_create(EPOLL_SIZE);
	CHK(ep_fd, "epoll create\n"); //TODO edit to use errno

	CHK(epoll_ctl(ep_fd, EPOLL_CTL_ADD, serv_fd, &event), "epoll ctl\n");

	for (;;)
	{
		n_events = epoll_wait(ep_fd, events, EPOLL_SIZE, -1);
		for (int i = 0; i < n_events; i++)
		{
			if (events[i].data.fd ==  serv_fd)
			{
				cl_fd = accept(serv_fd, (struct sockaddr *) &cl_sock, (socklen_t *) sizeof(cl_sock));
				CHK(cl_fd, "accept");

				string welcome = "Welcome\n";
				CHK(send(cl_fd, welcome.data(), welcome.size(), 0), "send welcome\n");
				setnonblock(cl_fd);
				clients.push_back(cl_fd);
				event.events = EPOLLIN | EPOLLET;
				event.data.fd = cl_fd;
				CHK(epoll_ctl(ep_fd, EPOLL_CTL_ADD, cl_fd, &event), "epoll ctl client\n");

			} else if (events[i].events & EPOLLIN)
			{
//				CHK(send_msg(), "send msg"); //TODO send_msg()
				int msg_len = recv(events[i].data.fd, buf, BUF_SIZE, 0);
				CHK(msg_len, "recieve msg error");
				//client closed connection
				if (msg_len == 0)
				{
					CHK(close(cl_fd), "closing client fd");
					//TODO delete clients fd
					for (uint i = 0; i < clients.size(); i++)
					{
						if (clients[i] == cl_fd)
						{
							clients.erase(clients.begin() + i);
						}
					}
					CHK(epoll_ctl(ep_fd, EPOLL_CTL_ADD, events[i].data.fd, &event), "delete cl_fd from clients\n");
				}
				//else send msg
				//TODO check if only 1 user (you)
				for (uint i = 0; i < clients.size(); i++) // TODO iterate over clients
				{
					CHK(send(clients[i], buf, BUF_SIZE, 0), "send msg to all clients\n");
				}

			} else if (events[i].events	& (EPOLLHUP | EPOLLERR))
			{
				close(events[i].data.fd);
				cout << "connection terminated\n";
				epoll_ctl(ep_fd, EPOLL_CTL_DEL, events[i].data.fd, &event);

			}
		}
	}
	close(serv_fd);
	close(ep_fd);
	return 0;
}











