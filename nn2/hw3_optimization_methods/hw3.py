import numpy as np
from collections import defaultdict

def step(theta, grad, method, state):
    methods = ('GD','Momentum', 'NAG', 'Adagrad', 'RMSprop', 'Adadelta', 'Adam')
    if method not in methods:
        print "No such method %s" % method
        return theta, state
    
    state = defaultdict(lambda : 0, state)

    if method == "GD":
        eta = state['eta']
        theta_new = theta -  eta * grad(theta)
        state_new = state
        return theta_new, state_new
    
    if method == "Momentum":
        gamma = state['gamma']
        eta = state['eta']
        v = state['v']
        v_new = gamma * v + eta * grad(theta)
        theta_new = theta - v_new
        state['v'] = v_new
        state_new = state
        return theta_new, state_new
    
    if method == "NAG":
        gamma = state['gamma']
        eta = state['eta']
        v = state['v']
        v_new = gamma * v + eta * grad(theta - gamma * v)
        theta_new = theta - v_new
        state['v'] = v_new
        state_new = state
        return theta_new, state_new
    
    if method == "Adagrad":
        G = state['G']
        eps = state['eps']
        eta = state['eta']
        gr = grad(theta)
        G += gr ** 2 if G != 0 else gr ** 2 
        theta_new = theta - eta * gr / np.sqrt(G + eps)  
        state['G'] = G
        state_new = state
        return theta_new, state_new
    
    if method == "RMSprop":
        gamma = state['gamma']
        eta = state['eta']
        E = state['E']
        eps = state['eps']
        gr = grad(theta)
        E_new = gamma * E + (1.0 - gamma) * gr ** 2
        theta_new  = theta - eta * gr / np.sqrt(E_new + eps)
        state['E'] = E_new
        state_new = state
        return theta_new, state_new
    
    if method == "Adadelta":
        eps = state['eps']
        gamma = state['gamma']
        Eth = state['Eth']
        Eg = state['Eg']
        dtheta = state['dtheta']
        RMSth = state['RMSth']
        gr = grad(theta)
        Eth_new = gamma * Eth + (1.0 - gamma) * dtheta ** 2
        Eg_new = gamma * Eg + (1.0 - gamma) * gr ** 2
        RMSth_new = np.sqrt(Eth_new + eps)
        RMSg_new = np.sqrt(Eg_new + eps)
        dtheta_new = RMSth / RMSg_new * gr
        theta_new = theta - dtheta_new
        state['Eth'] = Eth_new
        state['Eg'] = Eg_new
        state['RMSth'] =  RMSth_new
        state['dtheta'] = dtheta_new
        state_new = state
        return theta_new, state_new
    
    if method == "Adam":
        eta = state['eta']
        b1 = state['beta1']
        b2 = state['beta2']
        t = state['t'] if state['t'] != 0 else 1
        eps = state['eps']
        gamma = state['gamma']
        m = state['m']
        v = state['v']
        gr = grad(theta)
        m_new = b1 * m + (1.0 - b1) * gr
        v_new = b2 * v + (1.0 - b2) * gr ** 2
        mt = m_new / (1.0 - b1 ** t)
        vt = v_new / (1.0 - b2 ** t)
        theta_new = theta - eta / np.sqrt(vt + eps) * mt
        state['v'] = v_new
        state['m'] = m_new
        state['t'] = t + 1
        state_new = state
        return theta_new, state_new
    