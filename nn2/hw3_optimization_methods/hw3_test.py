import numpy as np
from collections import defaultdict
from hw3 import step
    
def init_state():
    state = dict()#defaultdict(lambda : 0.0)
    state['eta'] = 0.01
    state['gamma'] = 0.9
    state['eps'] = 1e-8
    state['beta1'] = 0.9
    state['beta2'] = 0.999
    return state

def loss(x):
    return np.sum(x**2)

def dloss(x):
    return np.sum(2 * x)

def check_opt(gap = 1000, grad = lambda x : 2 * x, theta = np.array([1.0 , 1.0]), state = init_state(), method = "GD", \
              y_min = np.array([0.0, 0.0]), delta_loss = 0.0000000001, n_epochs = 50000):
    print "Method :", method
    for i in xrange(n_epochs):
        prev_loss = loss(theta)
        theta, state = step(method=method, theta = theta, state = state, grad = dloss)
        curr_loss = loss(theta)
        if np.sum(np.abs(y_min - curr_loss)) < delta_loss:
            print "Theta ", theta
            print "Iter %d: f(x)= %0.10f" % (i, loss(theta))
            print "#########################################"
            return i
        if gap != 0:
            if i % 10 == 0 :  print "Iter %d: x=%0.3f f(x)= %0.10f" % (i, theta, loss(theta))
    print "Theta ", theta
    print "Iter %d: f(x)= %0.10f" % (n_epochs, loss(theta))
    print "#########################################"
    return n_epochs

loss_functions = (lambda x, y : x**2 - y**2, lambda x, y : x**2 + y**2, \
    lambda x, y : x**2 * np.sin(x) + y**2 * np.sin(y), lambda x, y : x**2 * np.sin(x**2) + y**2 * sin(y**2))
check_opt(gap = 0, grad = dloss, state = init_state(), method = 'GD')
check_opt(gap = 0, grad = dloss, state = init_state(), method = 'Momentum')
check_opt(gap = 0, grad = dloss, state = init_state(), method = 'NAG')
check_opt(gap = 0, grad = dloss, state = init_state(), method = 'Adagrad')
check_opt(gap = 0, grad = dloss, state = init_state(), method = 'RMSprop')
check_opt(gap = 0, grad = dloss, state = init_state(), method = 'Adadelta')
check_opt(gap = 0, grad = dloss, state = init_state(), method = 'Adam')